﻿namespace PokerSimulator
{
    partial class ReplayWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReplayWindow));
            this.label1 = new System.Windows.Forms.Label();
            this.Player6PositionBox = new System.Windows.Forms.PictureBox();
            this.Player5PositionBox = new System.Windows.Forms.PictureBox();
            this.Player4PositionBox = new System.Windows.Forms.PictureBox();
            this.Player3PositionBox = new System.Windows.Forms.PictureBox();
            this.Player2PositionBox = new System.Windows.Forms.PictureBox();
            this.Player1PositionBox = new System.Windows.Forms.PictureBox();
            this.Player5Card2Box = new System.Windows.Forms.PictureBox();
            this.Player3Card2Box = new System.Windows.Forms.PictureBox();
            this.Player5Card1Box = new System.Windows.Forms.PictureBox();
            this.Player3Card1Box = new System.Windows.Forms.PictureBox();
            this.Player6Card2Box = new System.Windows.Forms.PictureBox();
            this.Player2Card2Box = new System.Windows.Forms.PictureBox();
            this.Player6Card1Box = new System.Windows.Forms.PictureBox();
            this.Player2Card1Box = new System.Windows.Forms.PictureBox();
            this.Player4Card2Box = new System.Windows.Forms.PictureBox();
            this.Player4Card1Box = new System.Windows.Forms.PictureBox();
            this.Player1Card2Box = new System.Windows.Forms.PictureBox();
            this.RiverCardBox = new System.Windows.Forms.PictureBox();
            this.TurnCardBox = new System.Windows.Forms.PictureBox();
            this.FlopCard3Box = new System.Windows.Forms.PictureBox();
            this.FlopCard2Box = new System.Windows.Forms.PictureBox();
            this.FlopCard1Box = new System.Windows.Forms.PictureBox();
            this.Player1Card1Box = new System.Windows.Forms.PictureBox();
            this.player6Name = new System.Windows.Forms.Label();
            this.player5Name = new System.Windows.Forms.Label();
            this.player2Name = new System.Windows.Forms.Label();
            this.player3Name = new System.Windows.Forms.Label();
            this.player4Name = new System.Windows.Forms.Label();
            this.player1Name = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Player6PositionBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player5PositionBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4PositionBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3PositionBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2PositionBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1PositionBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player5Card2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Card2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player5Card1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Card1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player6Card2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Card2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player6Card1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Card1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Card2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Card1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Card2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RiverCardBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TurnCardBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FlopCard3Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FlopCard2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FlopCard1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Card1Box)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(540, 479);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 37;
            // 
            // Player6PositionBox
            // 
            this.Player6PositionBox.BackColor = System.Drawing.Color.Transparent;
            this.Player6PositionBox.Location = new System.Drawing.Point(725, 382);
            this.Player6PositionBox.Name = "Player6PositionBox";
            this.Player6PositionBox.Size = new System.Drawing.Size(30, 30);
            this.Player6PositionBox.TabIndex = 36;
            this.Player6PositionBox.TabStop = false;
            // 
            // Player5PositionBox
            // 
            this.Player5PositionBox.BackColor = System.Drawing.Color.Transparent;
            this.Player5PositionBox.Location = new System.Drawing.Point(725, 276);
            this.Player5PositionBox.Name = "Player5PositionBox";
            this.Player5PositionBox.Size = new System.Drawing.Size(30, 30);
            this.Player5PositionBox.TabIndex = 35;
            this.Player5PositionBox.TabStop = false;
            // 
            // Player4PositionBox
            // 
            this.Player4PositionBox.BackColor = System.Drawing.Color.Transparent;
            this.Player4PositionBox.Location = new System.Drawing.Point(425, 219);
            this.Player4PositionBox.Name = "Player4PositionBox";
            this.Player4PositionBox.Size = new System.Drawing.Size(30, 30);
            this.Player4PositionBox.TabIndex = 34;
            this.Player4PositionBox.TabStop = false;
            // 
            // Player3PositionBox
            // 
            this.Player3PositionBox.BackColor = System.Drawing.Color.Transparent;
            this.Player3PositionBox.Location = new System.Drawing.Point(247, 276);
            this.Player3PositionBox.Name = "Player3PositionBox";
            this.Player3PositionBox.Size = new System.Drawing.Size(30, 30);
            this.Player3PositionBox.TabIndex = 33;
            this.Player3PositionBox.TabStop = false;
            // 
            // Player2PositionBox
            // 
            this.Player2PositionBox.BackColor = System.Drawing.Color.Transparent;
            this.Player2PositionBox.Location = new System.Drawing.Point(247, 382);
            this.Player2PositionBox.Name = "Player2PositionBox";
            this.Player2PositionBox.Size = new System.Drawing.Size(30, 30);
            this.Player2PositionBox.TabIndex = 32;
            this.Player2PositionBox.TabStop = false;
            // 
            // Player1PositionBox
            // 
            this.Player1PositionBox.BackColor = System.Drawing.Color.Transparent;
            this.Player1PositionBox.Location = new System.Drawing.Point(425, 459);
            this.Player1PositionBox.Name = "Player1PositionBox";
            this.Player1PositionBox.Size = new System.Drawing.Size(30, 30);
            this.Player1PositionBox.TabIndex = 31;
            this.Player1PositionBox.TabStop = false;
            // 
            // Player5Card2Box
            // 
            this.Player5Card2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player5Card2Box.Image = ((System.Drawing.Image)(resources.GetObject("Player5Card2Box.Image")));
            this.Player5Card2Box.Location = new System.Drawing.Point(803, 180);
            this.Player5Card2Box.Name = "Player5Card2Box";
            this.Player5Card2Box.Size = new System.Drawing.Size(72, 90);
            this.Player5Card2Box.TabIndex = 29;
            this.Player5Card2Box.TabStop = false;
            // 
            // Player3Card2Box
            // 
            this.Player3Card2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player3Card2Box.Image = ((System.Drawing.Image)(resources.GetObject("Player3Card2Box.Image")));
            this.Player3Card2Box.Location = new System.Drawing.Point(205, 180);
            this.Player3Card2Box.Name = "Player3Card2Box";
            this.Player3Card2Box.Size = new System.Drawing.Size(72, 90);
            this.Player3Card2Box.TabIndex = 28;
            this.Player3Card2Box.TabStop = false;
            // 
            // Player5Card1Box
            // 
            this.Player5Card1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player5Card1Box.Image = ((System.Drawing.Image)(resources.GetObject("Player5Card1Box.Image")));
            this.Player5Card1Box.Location = new System.Drawing.Point(725, 180);
            this.Player5Card1Box.Name = "Player5Card1Box";
            this.Player5Card1Box.Size = new System.Drawing.Size(72, 90);
            this.Player5Card1Box.TabIndex = 27;
            this.Player5Card1Box.TabStop = false;
            // 
            // Player3Card1Box
            // 
            this.Player3Card1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player3Card1Box.Image = ((System.Drawing.Image)(resources.GetObject("Player3Card1Box.Image")));
            this.Player3Card1Box.Location = new System.Drawing.Point(127, 180);
            this.Player3Card1Box.Name = "Player3Card1Box";
            this.Player3Card1Box.Size = new System.Drawing.Size(72, 90);
            this.Player3Card1Box.TabIndex = 26;
            this.Player3Card1Box.TabStop = false;
            // 
            // Player6Card2Box
            // 
            this.Player6Card2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player6Card2Box.Image = ((System.Drawing.Image)(resources.GetObject("Player6Card2Box.Image")));
            this.Player6Card2Box.Location = new System.Drawing.Point(803, 418);
            this.Player6Card2Box.Name = "Player6Card2Box";
            this.Player6Card2Box.Size = new System.Drawing.Size(72, 90);
            this.Player6Card2Box.TabIndex = 25;
            this.Player6Card2Box.TabStop = false;
            // 
            // Player2Card2Box
            // 
            this.Player2Card2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player2Card2Box.Image = ((System.Drawing.Image)(resources.GetObject("Player2Card2Box.Image")));
            this.Player2Card2Box.Location = new System.Drawing.Point(205, 418);
            this.Player2Card2Box.Name = "Player2Card2Box";
            this.Player2Card2Box.Size = new System.Drawing.Size(72, 90);
            this.Player2Card2Box.TabIndex = 24;
            this.Player2Card2Box.TabStop = false;
            // 
            // Player6Card1Box
            // 
            this.Player6Card1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player6Card1Box.Image = ((System.Drawing.Image)(resources.GetObject("Player6Card1Box.Image")));
            this.Player6Card1Box.Location = new System.Drawing.Point(725, 418);
            this.Player6Card1Box.Name = "Player6Card1Box";
            this.Player6Card1Box.Size = new System.Drawing.Size(72, 90);
            this.Player6Card1Box.TabIndex = 30;
            this.Player6Card1Box.TabStop = false;
            // 
            // Player2Card1Box
            // 
            this.Player2Card1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player2Card1Box.Image = ((System.Drawing.Image)(resources.GetObject("Player2Card1Box.Image")));
            this.Player2Card1Box.Location = new System.Drawing.Point(127, 418);
            this.Player2Card1Box.Name = "Player2Card1Box";
            this.Player2Card1Box.Size = new System.Drawing.Size(72, 90);
            this.Player2Card1Box.TabIndex = 23;
            this.Player2Card1Box.TabStop = false;
            // 
            // Player4Card2Box
            // 
            this.Player4Card2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player4Card2Box.Image = ((System.Drawing.Image)(resources.GetObject("Player4Card2Box.Image")));
            this.Player4Card2Box.Location = new System.Drawing.Point(503, 123);
            this.Player4Card2Box.Name = "Player4Card2Box";
            this.Player4Card2Box.Size = new System.Drawing.Size(72, 90);
            this.Player4Card2Box.TabIndex = 22;
            this.Player4Card2Box.TabStop = false;
            // 
            // Player4Card1Box
            // 
            this.Player4Card1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player4Card1Box.Image = ((System.Drawing.Image)(resources.GetObject("Player4Card1Box.Image")));
            this.Player4Card1Box.Location = new System.Drawing.Point(425, 123);
            this.Player4Card1Box.Name = "Player4Card1Box";
            this.Player4Card1Box.Size = new System.Drawing.Size(72, 90);
            this.Player4Card1Box.TabIndex = 21;
            this.Player4Card1Box.TabStop = false;
            // 
            // Player1Card2Box
            // 
            this.Player1Card2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player1Card2Box.Image = global::PokerSimulator.Properties.Resources.BackSide;
            this.Player1Card2Box.Location = new System.Drawing.Point(503, 495);
            this.Player1Card2Box.Name = "Player1Card2Box";
            this.Player1Card2Box.Size = new System.Drawing.Size(72, 90);
            this.Player1Card2Box.TabIndex = 20;
            this.Player1Card2Box.TabStop = false;
            // 
            // RiverCardBox
            // 
            this.RiverCardBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.RiverCardBox.Image = ((System.Drawing.Image)(resources.GetObject("RiverCardBox.Image")));
            this.RiverCardBox.Location = new System.Drawing.Point(622, 299);
            this.RiverCardBox.Name = "RiverCardBox";
            this.RiverCardBox.Size = new System.Drawing.Size(72, 90);
            this.RiverCardBox.TabIndex = 19;
            this.RiverCardBox.TabStop = false;
            // 
            // TurnCardBox
            // 
            this.TurnCardBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TurnCardBox.Image = ((System.Drawing.Image)(resources.GetObject("TurnCardBox.Image")));
            this.TurnCardBox.Location = new System.Drawing.Point(544, 299);
            this.TurnCardBox.Name = "TurnCardBox";
            this.TurnCardBox.Size = new System.Drawing.Size(72, 90);
            this.TurnCardBox.TabIndex = 18;
            this.TurnCardBox.TabStop = false;
            // 
            // FlopCard3Box
            // 
            this.FlopCard3Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FlopCard3Box.Image = ((System.Drawing.Image)(resources.GetObject("FlopCard3Box.Image")));
            this.FlopCard3Box.Location = new System.Drawing.Point(466, 299);
            this.FlopCard3Box.Name = "FlopCard3Box";
            this.FlopCard3Box.Size = new System.Drawing.Size(72, 90);
            this.FlopCard3Box.TabIndex = 17;
            this.FlopCard3Box.TabStop = false;
            // 
            // FlopCard2Box
            // 
            this.FlopCard2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FlopCard2Box.Image = ((System.Drawing.Image)(resources.GetObject("FlopCard2Box.Image")));
            this.FlopCard2Box.Location = new System.Drawing.Point(388, 299);
            this.FlopCard2Box.Name = "FlopCard2Box";
            this.FlopCard2Box.Size = new System.Drawing.Size(72, 90);
            this.FlopCard2Box.TabIndex = 16;
            this.FlopCard2Box.TabStop = false;
            // 
            // FlopCard1Box
            // 
            this.FlopCard1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FlopCard1Box.Image = ((System.Drawing.Image)(resources.GetObject("FlopCard1Box.Image")));
            this.FlopCard1Box.Location = new System.Drawing.Point(310, 299);
            this.FlopCard1Box.Name = "FlopCard1Box";
            this.FlopCard1Box.Size = new System.Drawing.Size(72, 90);
            this.FlopCard1Box.TabIndex = 15;
            this.FlopCard1Box.TabStop = false;
            // 
            // Player1Card1Box
            // 
            this.Player1Card1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player1Card1Box.Image = global::PokerSimulator.Properties.Resources.BackSide;
            this.Player1Card1Box.Location = new System.Drawing.Point(425, 495);
            this.Player1Card1Box.Name = "Player1Card1Box";
            this.Player1Card1Box.Size = new System.Drawing.Size(72, 90);
            this.Player1Card1Box.TabIndex = 14;
            this.Player1Card1Box.TabStop = false;
            // 
            // player6Name
            // 
            this.player6Name.AutoSize = true;
            this.player6Name.BackColor = System.Drawing.Color.DimGray;
            this.player6Name.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.player6Name.ForeColor = System.Drawing.Color.LawnGreen;
            this.player6Name.Location = new System.Drawing.Point(779, 511);
            this.player6Name.Name = "player6Name";
            this.player6Name.Size = new System.Drawing.Size(47, 14);
            this.player6Name.TabIndex = 12;
            this.player6Name.Text = "Player6";
            // 
            // player5Name
            // 
            this.player5Name.AutoSize = true;
            this.player5Name.BackColor = System.Drawing.Color.DimGray;
            this.player5Name.ForeColor = System.Drawing.Color.LawnGreen;
            this.player5Name.Location = new System.Drawing.Point(784, 164);
            this.player5Name.Name = "player5Name";
            this.player5Name.Size = new System.Drawing.Size(42, 13);
            this.player5Name.TabIndex = 11;
            this.player5Name.Text = "Player5";
            // 
            // player2Name
            // 
            this.player2Name.AutoSize = true;
            this.player2Name.BackColor = System.Drawing.Color.DimGray;
            this.player2Name.ForeColor = System.Drawing.Color.LawnGreen;
            this.player2Name.Location = new System.Drawing.Point(180, 511);
            this.player2Name.Name = "player2Name";
            this.player2Name.Size = new System.Drawing.Size(42, 13);
            this.player2Name.TabIndex = 10;
            this.player2Name.Text = "Player2";
            // 
            // player3Name
            // 
            this.player3Name.AutoSize = true;
            this.player3Name.BackColor = System.Drawing.Color.DimGray;
            this.player3Name.ForeColor = System.Drawing.Color.LawnGreen;
            this.player3Name.Location = new System.Drawing.Point(180, 164);
            this.player3Name.Name = "player3Name";
            this.player3Name.Size = new System.Drawing.Size(42, 13);
            this.player3Name.TabIndex = 9;
            this.player3Name.Text = "Player3";
            // 
            // player4Name
            // 
            this.player4Name.AutoSize = true;
            this.player4Name.BackColor = System.Drawing.Color.DimGray;
            this.player4Name.ForeColor = System.Drawing.Color.LawnGreen;
            this.player4Name.Location = new System.Drawing.Point(483, 107);
            this.player4Name.Name = "player4Name";
            this.player4Name.Size = new System.Drawing.Size(42, 13);
            this.player4Name.TabIndex = 13;
            this.player4Name.Text = "Player4";
            // 
            // player1Name
            // 
            this.player1Name.AutoSize = true;
            this.player1Name.BackColor = System.Drawing.Color.DimGray;
            this.player1Name.ForeColor = System.Drawing.Color.LawnGreen;
            this.player1Name.Location = new System.Drawing.Point(483, 588);
            this.player1Name.Name = "player1Name";
            this.player1Name.Size = new System.Drawing.Size(42, 13);
            this.player1Name.TabIndex = 8;
            this.player1Name.Tag = "pl1n";
            this.player1Name.Text = "Player1";
            // 
            // ReplayWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PokerSimulator.Properties.Resources.PokerTable_replay;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Player6PositionBox);
            this.Controls.Add(this.Player5PositionBox);
            this.Controls.Add(this.Player4PositionBox);
            this.Controls.Add(this.Player3PositionBox);
            this.Controls.Add(this.Player2PositionBox);
            this.Controls.Add(this.Player1PositionBox);
            this.Controls.Add(this.Player5Card2Box);
            this.Controls.Add(this.Player3Card2Box);
            this.Controls.Add(this.Player5Card1Box);
            this.Controls.Add(this.Player3Card1Box);
            this.Controls.Add(this.Player6Card2Box);
            this.Controls.Add(this.Player2Card2Box);
            this.Controls.Add(this.Player6Card1Box);
            this.Controls.Add(this.Player2Card1Box);
            this.Controls.Add(this.Player4Card2Box);
            this.Controls.Add(this.Player4Card1Box);
            this.Controls.Add(this.Player1Card2Box);
            this.Controls.Add(this.RiverCardBox);
            this.Controls.Add(this.TurnCardBox);
            this.Controls.Add(this.FlopCard3Box);
            this.Controls.Add(this.FlopCard2Box);
            this.Controls.Add(this.FlopCard1Box);
            this.Controls.Add(this.Player1Card1Box);
            this.Controls.Add(this.player6Name);
            this.Controls.Add(this.player5Name);
            this.Controls.Add(this.player2Name);
            this.Controls.Add(this.player3Name);
            this.Controls.Add(this.player4Name);
            this.Controls.Add(this.player1Name);
            this.Location = new System.Drawing.Point(400, 200);
            this.Name = "ReplayWindow";
            this.Text = "Replay";
            ((System.ComponentModel.ISupportInitialize)(this.Player6PositionBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player5PositionBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4PositionBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3PositionBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2PositionBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1PositionBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player5Card2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Card2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player5Card1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Card1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player6Card2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Card2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player6Card1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Card1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Card2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Card1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Card2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RiverCardBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TurnCardBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FlopCard3Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FlopCard2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FlopCard1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Card1Box)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox Player6PositionBox;
        private System.Windows.Forms.PictureBox Player5PositionBox;
        private System.Windows.Forms.PictureBox Player4PositionBox;
        private System.Windows.Forms.PictureBox Player3PositionBox;
        private System.Windows.Forms.PictureBox Player2PositionBox;
        private System.Windows.Forms.PictureBox Player1PositionBox;
        private System.Windows.Forms.PictureBox Player5Card2Box;
        private System.Windows.Forms.PictureBox Player3Card2Box;
        private System.Windows.Forms.PictureBox Player5Card1Box;
        private System.Windows.Forms.PictureBox Player3Card1Box;
        private System.Windows.Forms.PictureBox Player6Card2Box;
        private System.Windows.Forms.PictureBox Player2Card2Box;
        private System.Windows.Forms.PictureBox Player6Card1Box;
        private System.Windows.Forms.PictureBox Player2Card1Box;
        private System.Windows.Forms.PictureBox Player4Card2Box;
        private System.Windows.Forms.PictureBox Player4Card1Box;
        private System.Windows.Forms.PictureBox Player1Card2Box;
        private System.Windows.Forms.PictureBox RiverCardBox;
        private System.Windows.Forms.PictureBox TurnCardBox;
        private System.Windows.Forms.PictureBox FlopCard3Box;
        private System.Windows.Forms.PictureBox FlopCard2Box;
        private System.Windows.Forms.PictureBox FlopCard1Box;
        private System.Windows.Forms.PictureBox Player1Card1Box;
        private System.Windows.Forms.Label player6Name;
        private System.Windows.Forms.Label player5Name;
        private System.Windows.Forms.Label player2Name;
        private System.Windows.Forms.Label player3Name;
        private System.Windows.Forms.Label player4Name;
        private System.Windows.Forms.Label player1Name;
    }
}