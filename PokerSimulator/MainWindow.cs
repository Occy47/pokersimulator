﻿using System;
using System.Drawing;
using System.Windows.Forms;
using HoldemTable;
using HoldemRepositoryCSV;
using HoldemRepositorySQL;
using System.IO;

namespace PokerSimulator
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            table.ShuffleCardDeck();
            table.SetPositions();
        }

        private readonly Table table = new Table();
        private GameRules rules = new GameRules();
        private SQLManager dbcont = new SQLManager();

        //players cards
        Card PicBox1;
        Card PicBox2;
        Card PicBox3;
        Card PicBox4;
        Card PicBox5;
        Card PicBox6;
        Card PicBox7;
        Card PicBox8;
        Card PicBox9;
        Card PicBox10;
        Card PicBox11;
        Card PicBox12;

        //board cards 
        Card PicBox13;
        Card PicBox14;
        Card PicBox15;
        Card PicBox16;
        Card PicBox17;

        private Image dealerPosition = PokerSimulator.Properties.Resources.Button;

        public void SetPickBoxCards()
        {

            PicBox1 = table.Players[0].Card1;
            PicBox2 = table.Players[0].Card2;
            PicBox3 = table.Players[1].Card1;
            PicBox4 = table.Players[1].Card2;
            PicBox5 = table.Players[2].Card1;
            PicBox6 = table.Players[2].Card2;
            PicBox7 = table.Players[3].Card1;
            PicBox8 = table.Players[3].Card2;
            PicBox9 = table.Players[4].Card1;
            PicBox10 = table.Players[4].Card2;
            PicBox11 = table.Players[5].Card1;
            PicBox12 = table.Players[5].Card2;
            PicBox13 = table.Board.Card1;
            PicBox14 = table.Board.Card2;
            PicBox15 = table.Board.Card3;
            PicBox16 = table.Board.Card4;
            PicBox17 = table.Board.Card5;


            rules.SetPlayers(table.Players[0], table.Board);
            label1.Text = rules.DisplayLabelText();
        }

        private void DealCardsToPlayers_Click(object sender, EventArgs e)
        {
            var repo = new RepositoryCSV();

            //Directory.SetCurrentDirectory(Environment.ExpandEnvironmentVariables(@"%USERPROFILE%\Desktop\"));
            //repo.SaveArrayAsCSV(table.ShuffledDeck, "decks");
            //gameIndex++;
            //table.GameStatus(gameIndex);

            table.SetPlayers();
            table.AssignCardsToBoard();
            SetPickBoxCards();
            SetDealerImage();
            dbcont.SaveGameCards(table.Players, table.Board);

            Player1Card1Box.Image = GetImagePath(PicBox1.Rank, PicBox1.Suite);
            Player1Card2Box.Image = GetImagePath(PicBox2.Rank, PicBox2.Suite);

            Player2Card1Box.Image = GetImagePath(PicBox3.Rank, PicBox3.Suite);
            Player2Card2Box.Image = GetImagePath(PicBox4.Rank, PicBox4.Suite);

            Player3Card1Box.Image = GetImagePath(PicBox5.Rank, PicBox5.Suite);
            Player3Card2Box.Image = GetImagePath(PicBox6.Rank, PicBox6.Suite);

            Player4Card1Box.Image = GetImagePath(PicBox7.Rank, PicBox7.Suite);
            Player4Card2Box.Image = GetImagePath(PicBox8.Rank, PicBox8.Suite);

            Player5Card1Box.Image = GetImagePath(PicBox9.Rank, PicBox9.Suite);
            Player5Card2Box.Image = GetImagePath(PicBox10.Rank, PicBox10.Suite);

            Player6Card1Box.Image = GetImagePath(PicBox11.Rank, PicBox11.Suite);
            Player6Card2Box.Image = GetImagePath(PicBox12.Rank, PicBox12.Suite);

        }

        private System.Drawing.Bitmap GetImagePath(Card.CardRank r, Card.CardSuite s)
        {
          
            if (s == Card.CardSuite.Hearts && r == Card.CardRank.Two )
                return PokerSimulator.Properties.Resources.TwoOfHearts;
            else if (s == Card.CardSuite.Hearts && r == Card.CardRank.Three)
                return PokerSimulator.Properties.Resources.ThreeOfHearts;
            else if (s == Card.CardSuite.Hearts && r == Card.CardRank.Four)
                return PokerSimulator.Properties.Resources.FourOfHearts;
            else if (s == Card.CardSuite.Hearts && r == Card.CardRank.Five)
                return PokerSimulator.Properties.Resources.FiveOfHearts;
            else if (s == Card.CardSuite.Hearts && r == Card.CardRank.Six)
                return PokerSimulator.Properties.Resources.SixOfHearts;
            else if (s == Card.CardSuite.Hearts && r == Card.CardRank.Seven)
                return PokerSimulator.Properties.Resources.SevenOfHearts;
            else if (s == Card.CardSuite.Hearts && r == Card.CardRank.Eight)
                return PokerSimulator.Properties.Resources.EightOfHearts;
            else if (s == Card.CardSuite.Hearts && r == Card.CardRank.Nine)
                return PokerSimulator.Properties.Resources.NineOfHearts;
            else if (s == Card.CardSuite.Hearts && r == Card.CardRank.Ten)
                return PokerSimulator.Properties.Resources.TenOfHearts;
            else if (s == Card.CardSuite.Hearts && r == Card.CardRank.Jack)
                return PokerSimulator.Properties.Resources.JackOfHearts;
            else if (s == Card.CardSuite.Hearts && r == Card.CardRank.Queen)
                return PokerSimulator.Properties.Resources.QueenOfHearts;
            else if (s == Card.CardSuite.Hearts && r == Card.CardRank.King)
                return PokerSimulator.Properties.Resources.KingOfHearts;
            else if (s == Card.CardSuite.Hearts && r == Card.CardRank.Ace)
                return PokerSimulator.Properties.Resources.AceOfHearts;
            else if (s == Card.CardSuite.Spades && r == Card.CardRank.Two)
                return PokerSimulator.Properties.Resources.TwoOfSpades;
            else if (s == Card.CardSuite.Spades && r == Card.CardRank.Three)
                return PokerSimulator.Properties.Resources.ThreeOfSpades;
            else if (s == Card.CardSuite.Spades && r == Card.CardRank.Four)
                return PokerSimulator.Properties.Resources.FourOfSpades;
            else if (s == Card.CardSuite.Spades && r == Card.CardRank.Five)
                return PokerSimulator.Properties.Resources.FiveOfSpades;
            else if (s == Card.CardSuite.Spades && r == Card.CardRank.Six)
                return PokerSimulator.Properties.Resources.SixOfSpades;
            else if (s == Card.CardSuite.Spades && r == Card.CardRank.Seven)
                return PokerSimulator.Properties.Resources.SevenOfSpades;
            else if (s == Card.CardSuite.Spades && r == Card.CardRank.Eight)
                return PokerSimulator.Properties.Resources.EightOfSpades;
            else if (s == Card.CardSuite.Spades && r == Card.CardRank.Nine)
                return PokerSimulator.Properties.Resources.NineOfSpades;
            else if (s == Card.CardSuite.Spades && r == Card.CardRank.Ten)
                return PokerSimulator.Properties.Resources.TenOfSpades;
            else if (s == Card.CardSuite.Spades && r == Card.CardRank.Jack)
                return PokerSimulator.Properties.Resources.JackOfSpades;
            else if (s == Card.CardSuite.Spades && r == Card.CardRank.Queen)
                return PokerSimulator.Properties.Resources.QueenOfSpades;
            else if (s == Card.CardSuite.Spades && r == Card.CardRank.King)
                return PokerSimulator.Properties.Resources.KingOfSpades;
            else if (s == Card.CardSuite.Spades && r == Card.CardRank.Ace)
                return PokerSimulator.Properties.Resources.AceOfSpades;
            else if (s == Card.CardSuite.Clubs && r== Card.CardRank.Two)
                return PokerSimulator.Properties.Resources.TwoOfClubs;
            else if (s == Card.CardSuite.Clubs && r == Card.CardRank.Three)
                return PokerSimulator.Properties.Resources.ThreeOfClubs;
            else if (s == Card.CardSuite.Clubs && r == Card.CardRank.Four)
                return PokerSimulator.Properties.Resources.FourOfClubs;
            else if (s == Card.CardSuite.Clubs && r == Card.CardRank.Five)   
                return PokerSimulator.Properties.Resources.FiveOfClubs;
            else if (s == Card.CardSuite.Clubs && r == Card.CardRank.Six)
                return PokerSimulator.Properties.Resources.SixOfClubs;
            else if (s == Card.CardSuite.Clubs && r == Card.CardRank.Seven)
                return PokerSimulator.Properties.Resources.SevenOfClubs;
            else if (s == Card.CardSuite.Clubs && r == Card.CardRank.Eight)
                return PokerSimulator.Properties.Resources.EightOfClubs;
            else if (s == Card.CardSuite.Clubs && r == Card.CardRank.Nine)
                return PokerSimulator.Properties.Resources.NineOfClubs;
            else if (s == Card.CardSuite.Clubs && r == Card.CardRank.Ten)
                return PokerSimulator.Properties.Resources.TenOfClubs;
            else if (s == Card.CardSuite.Clubs && r == Card.CardRank.Jack)
                return PokerSimulator.Properties.Resources.JackOfClubs;
            else if (s == Card.CardSuite.Clubs && r == Card.CardRank.Queen)
                return PokerSimulator.Properties.Resources.QueenOfClubs;
            else if (s == Card.CardSuite.Clubs && r == Card.CardRank.King)
                return PokerSimulator.Properties.Resources.KingOfClubs;
            else if (s == Card.CardSuite.Clubs && r == Card.CardRank.Ace)
                return PokerSimulator.Properties.Resources.AceOfClubs;
            else if (s== Card.CardSuite.Diamonds && r == Card.CardRank.Two)
                return PokerSimulator.Properties.Resources.TwoOfDiamonds;
            else if (s == Card.CardSuite.Diamonds && r == Card.CardRank.Three)
                return PokerSimulator.Properties.Resources.ThreeOfDiamonds;
            else if (s == Card.CardSuite.Diamonds && r == Card.CardRank.Four)
                return PokerSimulator.Properties.Resources.FourOfDiamonds;
            else if (s == Card.CardSuite.Diamonds && r == Card.CardRank.Five)
                return PokerSimulator.Properties.Resources.FiveOfDiamonds;
            else if (s == Card.CardSuite.Diamonds && r == Card.CardRank.Six)
                return PokerSimulator.Properties.Resources.SixOfDiamonds;
            else if (s == Card.CardSuite.Diamonds && r == Card.CardRank.Seven)
                return PokerSimulator.Properties.Resources.SevenOfDiamonds;
            else if (s == Card.CardSuite.Diamonds && r == Card.CardRank.Eight)
                return PokerSimulator.Properties.Resources.EightOfDiamonds;
            else if (s == Card.CardSuite.Diamonds && r == Card.CardRank.Nine)
                return PokerSimulator.Properties.Resources.NineOfDiamonds;
            else if (s == Card.CardSuite.Diamonds && r == Card.CardRank.Ten)
                return PokerSimulator.Properties.Resources.TenOfDiamonds;
            else if (s == Card.CardSuite.Diamonds && r == Card.CardRank.Jack)
                return PokerSimulator.Properties.Resources.JackOfDiamonds;
            else if (s == Card.CardSuite.Diamonds && r == Card.CardRank.Queen)
                return PokerSimulator.Properties.Resources.QueenOfDiamonds;
            else if (s == Card.CardSuite.Diamonds && r == Card.CardRank.King)
                return PokerSimulator.Properties.Resources.KingOfDiamonds;
            else if (s == Card.CardSuite.Diamonds && r == Card.CardRank.Ace)
                return PokerSimulator.Properties.Resources.AceOfDiamonds;
            else
                return PokerSimulator.Properties.Resources.BackSide;

        }

        private void DealFlopCards_Click(object sender, EventArgs e)
        {

            FlopCard1Box.Image = GetImagePath(PicBox13.Rank, PicBox13.Suite);
            FlopCard2Box.Image = GetImagePath(PicBox14.Rank, PicBox14.Suite);
            FlopCard3Box.Image = GetImagePath(PicBox15.Rank, PicBox15.Suite);
        }

        private void ShuffleDeck_Click(object sender, EventArgs e)
        {

            table.ShuffleCardDeck();
            table.SetNextPosition();
        }

        private void DealTurnCard_Click(object sender, EventArgs e)
        {

            TurnCardBox.Image = GetImagePath(PicBox16.Rank, PicBox16.Suite);
        }

        private void DealRiverCard_Click(object sender, EventArgs e)
        {

            RiverCardBox.Image = GetImagePath(PicBox17.Rank, PicBox17.Suite);
        }

        private void SetDealerImage()
        {
            if (table.currentPosition[5] == Table.PlayerPosition.Dealer)
                Player1PositionBox.Image = dealerPosition;
            else if (table.currentPosition[4] == Table.PlayerPosition.Dealer)
                Player2PositionBox.Image = dealerPosition;
            else if (table.currentPosition[3] == Table.PlayerPosition.Dealer)
                Player3PositionBox.Image = dealerPosition;
            else if (table.currentPosition[2] == Table.PlayerPosition.Dealer)
                Player4PositionBox.Image = dealerPosition;
            else if (table.currentPosition[1] == Table.PlayerPosition.Dealer)
                Player5PositionBox.Image = dealerPosition;
            else if (table.currentPosition[0] == Table.PlayerPosition.Dealer)
                Player6PositionBox.Image = dealerPosition;

        }

        private void ClearAllCards_Click(object sender, EventArgs e)
        {
            Player1Card1Box.Image = PokerSimulator.Properties.Resources.BackSide;
            Player1Card2Box.Image = PokerSimulator.Properties.Resources.BackSide;
            Player2Card1Box.Image = PokerSimulator.Properties.Resources.BackSide;
            Player2Card2Box.Image = PokerSimulator.Properties.Resources.BackSide;
            Player3Card1Box.Image = PokerSimulator.Properties.Resources.BackSide;
            Player3Card2Box.Image = PokerSimulator.Properties.Resources.BackSide;
            Player4Card1Box.Image = PokerSimulator.Properties.Resources.BackSide;
            Player4Card2Box.Image = PokerSimulator.Properties.Resources.BackSide;
            Player5Card1Box.Image = PokerSimulator.Properties.Resources.BackSide;
            Player5Card2Box.Image = PokerSimulator.Properties.Resources.BackSide;
            Player6Card1Box.Image = PokerSimulator.Properties.Resources.BackSide;
            Player6Card2Box.Image = PokerSimulator.Properties.Resources.BackSide;

            FlopCard1Box.Image = PokerSimulator.Properties.Resources.BackSide;
            FlopCard2Box.Image = PokerSimulator.Properties.Resources.BackSide;
            FlopCard3Box.Image = PokerSimulator.Properties.Resources.BackSide;

            TurnCardBox.Image = PokerSimulator.Properties.Resources.BackSide;

            RiverCardBox.Image = PokerSimulator.Properties.Resources.BackSide;

            Player1PositionBox.Image = null;
            Player2PositionBox.Image = null;
            Player3PositionBox.Image = null;
            Player4PositionBox.Image = null;
            Player5PositionBox.Image = null;
            Player6PositionBox.Image = null;
        }

        private void replayWindowButton_Click(object sender, EventArgs e)
        {
            ReplayWindow rp = new ReplayWindow();

            rp.Show();
        }
    }
}
