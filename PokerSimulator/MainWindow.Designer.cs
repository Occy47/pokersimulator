﻿namespace PokerSimulator
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.DealCardsToPlayers = new System.Windows.Forms.Button();
            this.player1Name = new System.Windows.Forms.Label();
            this.player4Name = new System.Windows.Forms.Label();
            this.player3Name = new System.Windows.Forms.Label();
            this.player2Name = new System.Windows.Forms.Label();
            this.player5Name = new System.Windows.Forms.Label();
            this.player6Name = new System.Windows.Forms.Label();
            this.DealFlopCards = new System.Windows.Forms.Button();
            this.ShuffleDeck = new System.Windows.Forms.Button();
            this.DealTurnCard = new System.Windows.Forms.Button();
            this.DealRiverCard = new System.Windows.Forms.Button();
            this.ClearAllCards = new System.Windows.Forms.Button();
            this.Player5Card2Box = new System.Windows.Forms.PictureBox();
            this.Player3Card2Box = new System.Windows.Forms.PictureBox();
            this.Player5Card1Box = new System.Windows.Forms.PictureBox();
            this.Player3Card1Box = new System.Windows.Forms.PictureBox();
            this.Player6Card2Box = new System.Windows.Forms.PictureBox();
            this.Player2Card2Box = new System.Windows.Forms.PictureBox();
            this.Player6Card1Box = new System.Windows.Forms.PictureBox();
            this.Player2Card1Box = new System.Windows.Forms.PictureBox();
            this.Player4Card2Box = new System.Windows.Forms.PictureBox();
            this.Player4Card1Box = new System.Windows.Forms.PictureBox();
            this.Player1Card2Box = new System.Windows.Forms.PictureBox();
            this.RiverCardBox = new System.Windows.Forms.PictureBox();
            this.TurnCardBox = new System.Windows.Forms.PictureBox();
            this.FlopCard3Box = new System.Windows.Forms.PictureBox();
            this.FlopCard2Box = new System.Windows.Forms.PictureBox();
            this.FlopCard1Box = new System.Windows.Forms.PictureBox();
            this.Player1Card1Box = new System.Windows.Forms.PictureBox();
            this.Player1PositionBox = new System.Windows.Forms.PictureBox();
            this.Player2PositionBox = new System.Windows.Forms.PictureBox();
            this.Player3PositionBox = new System.Windows.Forms.PictureBox();
            this.Player4PositionBox = new System.Windows.Forms.PictureBox();
            this.Player5PositionBox = new System.Windows.Forms.PictureBox();
            this.Player6PositionBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.replayWindowButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Player5Card2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Card2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player5Card1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Card1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player6Card2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Card2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player6Card1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Card1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Card2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Card1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Card2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RiverCardBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TurnCardBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FlopCard3Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FlopCard2Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FlopCard1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Card1Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1PositionBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2PositionBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3PositionBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4PositionBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player5PositionBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player6PositionBox)).BeginInit();
            this.SuspendLayout();
            // 
            // DealCardsToPlayers
            // 
            this.DealCardsToPlayers.BackColor = System.Drawing.Color.Red;
            this.DealCardsToPlayers.ForeColor = System.Drawing.Color.Yellow;
            this.DealCardsToPlayers.Location = new System.Drawing.Point(150, 662);
            this.DealCardsToPlayers.Name = "DealCardsToPlayers";
            this.DealCardsToPlayers.Size = new System.Drawing.Size(89, 35);
            this.DealCardsToPlayers.TabIndex = 0;
            this.DealCardsToPlayers.Text = "Deal";
            this.DealCardsToPlayers.UseVisualStyleBackColor = false;
            this.DealCardsToPlayers.Click += new System.EventHandler(this.DealCardsToPlayers_Click);
            // 
            // player1Name
            // 
            this.player1Name.AutoSize = true;
            this.player1Name.BackColor = System.Drawing.Color.DimGray;
            this.player1Name.ForeColor = System.Drawing.Color.LawnGreen;
            this.player1Name.Location = new System.Drawing.Point(484, 588);
            this.player1Name.Name = "player1Name";
            this.player1Name.Size = new System.Drawing.Size(42, 13);
            this.player1Name.TabIndex = 1;
            this.player1Name.Tag = "pl1n";
            this.player1Name.Text = "Player1";
            // 
            // player4Name
            // 
            this.player4Name.AutoSize = true;
            this.player4Name.BackColor = System.Drawing.Color.DimGray;
            this.player4Name.ForeColor = System.Drawing.Color.LawnGreen;
            this.player4Name.Location = new System.Drawing.Point(484, 107);
            this.player4Name.Name = "player4Name";
            this.player4Name.Size = new System.Drawing.Size(42, 13);
            this.player4Name.TabIndex = 1;
            this.player4Name.Text = "Player4";
            // 
            // player3Name
            // 
            this.player3Name.AutoSize = true;
            this.player3Name.BackColor = System.Drawing.Color.DimGray;
            this.player3Name.ForeColor = System.Drawing.Color.LawnGreen;
            this.player3Name.Location = new System.Drawing.Point(181, 164);
            this.player3Name.Name = "player3Name";
            this.player3Name.Size = new System.Drawing.Size(42, 13);
            this.player3Name.TabIndex = 1;
            this.player3Name.Text = "Player3";
            // 
            // player2Name
            // 
            this.player2Name.AutoSize = true;
            this.player2Name.BackColor = System.Drawing.Color.DimGray;
            this.player2Name.ForeColor = System.Drawing.Color.LawnGreen;
            this.player2Name.Location = new System.Drawing.Point(181, 511);
            this.player2Name.Name = "player2Name";
            this.player2Name.Size = new System.Drawing.Size(42, 13);
            this.player2Name.TabIndex = 1;
            this.player2Name.Text = "Player2";
            // 
            // player5Name
            // 
            this.player5Name.AutoSize = true;
            this.player5Name.BackColor = System.Drawing.Color.DimGray;
            this.player5Name.ForeColor = System.Drawing.Color.LawnGreen;
            this.player5Name.Location = new System.Drawing.Point(785, 164);
            this.player5Name.Name = "player5Name";
            this.player5Name.Size = new System.Drawing.Size(42, 13);
            this.player5Name.TabIndex = 1;
            this.player5Name.Text = "Player5";
            // 
            // player6Name
            // 
            this.player6Name.AutoSize = true;
            this.player6Name.BackColor = System.Drawing.Color.DimGray;
            this.player6Name.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.player6Name.ForeColor = System.Drawing.Color.LawnGreen;
            this.player6Name.Location = new System.Drawing.Point(780, 511);
            this.player6Name.Name = "player6Name";
            this.player6Name.Size = new System.Drawing.Size(47, 14);
            this.player6Name.TabIndex = 1;
            this.player6Name.Text = "Player6";
            // 
            // DealFlopCards
            // 
            this.DealFlopCards.BackColor = System.Drawing.Color.Red;
            this.DealFlopCards.ForeColor = System.Drawing.Color.Yellow;
            this.DealFlopCards.Location = new System.Drawing.Point(359, 662);
            this.DealFlopCards.Name = "DealFlopCards";
            this.DealFlopCards.Size = new System.Drawing.Size(89, 35);
            this.DealFlopCards.TabIndex = 0;
            this.DealFlopCards.Text = "Flop";
            this.DealFlopCards.UseVisualStyleBackColor = false;
            this.DealFlopCards.Click += new System.EventHandler(this.DealFlopCards_Click);
            // 
            // ShuffleDeck
            // 
            this.ShuffleDeck.BackColor = System.Drawing.Color.Red;
            this.ShuffleDeck.ForeColor = System.Drawing.Color.Yellow;
            this.ShuffleDeck.Location = new System.Drawing.Point(55, 662);
            this.ShuffleDeck.Name = "ShuffleDeck";
            this.ShuffleDeck.Size = new System.Drawing.Size(89, 35);
            this.ShuffleDeck.TabIndex = 0;
            this.ShuffleDeck.Text = "Shuffle";
            this.ShuffleDeck.UseVisualStyleBackColor = false;
            this.ShuffleDeck.Click += new System.EventHandler(this.ShuffleDeck_Click);
            // 
            // DealTurnCard
            // 
            this.DealTurnCard.BackColor = System.Drawing.Color.Red;
            this.DealTurnCard.ForeColor = System.Drawing.Color.Yellow;
            this.DealTurnCard.Location = new System.Drawing.Point(467, 662);
            this.DealTurnCard.Name = "DealTurnCard";
            this.DealTurnCard.Size = new System.Drawing.Size(89, 35);
            this.DealTurnCard.TabIndex = 0;
            this.DealTurnCard.Text = "Turn";
            this.DealTurnCard.UseVisualStyleBackColor = false;
            this.DealTurnCard.Click += new System.EventHandler(this.DealTurnCard_Click);
            // 
            // DealRiverCard
            // 
            this.DealRiverCard.BackColor = System.Drawing.Color.Red;
            this.DealRiverCard.ForeColor = System.Drawing.Color.Yellow;
            this.DealRiverCard.Location = new System.Drawing.Point(575, 662);
            this.DealRiverCard.Name = "DealRiverCard";
            this.DealRiverCard.Size = new System.Drawing.Size(89, 35);
            this.DealRiverCard.TabIndex = 0;
            this.DealRiverCard.Text = "River";
            this.DealRiverCard.UseVisualStyleBackColor = false;
            this.DealRiverCard.Click += new System.EventHandler(this.DealRiverCard_Click);
            // 
            // ClearAllCards
            // 
            this.ClearAllCards.BackColor = System.Drawing.Color.Red;
            this.ClearAllCards.ForeColor = System.Drawing.Color.Yellow;
            this.ClearAllCards.Location = new System.Drawing.Point(726, 662);
            this.ClearAllCards.Name = "ClearAllCards";
            this.ClearAllCards.Size = new System.Drawing.Size(89, 35);
            this.ClearAllCards.TabIndex = 0;
            this.ClearAllCards.Text = "Clear";
            this.ClearAllCards.UseVisualStyleBackColor = false;
            this.ClearAllCards.Click += new System.EventHandler(this.ClearAllCards_Click);
            // 
            // Player5Card2Box
            // 
            this.Player5Card2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player5Card2Box.Image = ((System.Drawing.Image)(resources.GetObject("Player5Card2Box.Image")));
            this.Player5Card2Box.Location = new System.Drawing.Point(804, 180);
            this.Player5Card2Box.Name = "Player5Card2Box";
            this.Player5Card2Box.Size = new System.Drawing.Size(72, 90);
            this.Player5Card2Box.TabIndex = 3;
            this.Player5Card2Box.TabStop = false;
            // 
            // Player3Card2Box
            // 
            this.Player3Card2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player3Card2Box.Image = ((System.Drawing.Image)(resources.GetObject("Player3Card2Box.Image")));
            this.Player3Card2Box.Location = new System.Drawing.Point(206, 180);
            this.Player3Card2Box.Name = "Player3Card2Box";
            this.Player3Card2Box.Size = new System.Drawing.Size(72, 90);
            this.Player3Card2Box.TabIndex = 3;
            this.Player3Card2Box.TabStop = false;
            // 
            // Player5Card1Box
            // 
            this.Player5Card1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player5Card1Box.Image = ((System.Drawing.Image)(resources.GetObject("Player5Card1Box.Image")));
            this.Player5Card1Box.Location = new System.Drawing.Point(726, 180);
            this.Player5Card1Box.Name = "Player5Card1Box";
            this.Player5Card1Box.Size = new System.Drawing.Size(72, 90);
            this.Player5Card1Box.TabIndex = 3;
            this.Player5Card1Box.TabStop = false;
            // 
            // Player3Card1Box
            // 
            this.Player3Card1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player3Card1Box.Image = ((System.Drawing.Image)(resources.GetObject("Player3Card1Box.Image")));
            this.Player3Card1Box.Location = new System.Drawing.Point(128, 180);
            this.Player3Card1Box.Name = "Player3Card1Box";
            this.Player3Card1Box.Size = new System.Drawing.Size(72, 90);
            this.Player3Card1Box.TabIndex = 3;
            this.Player3Card1Box.TabStop = false;
            // 
            // Player6Card2Box
            // 
            this.Player6Card2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player6Card2Box.Image = ((System.Drawing.Image)(resources.GetObject("Player6Card2Box.Image")));
            this.Player6Card2Box.Location = new System.Drawing.Point(804, 418);
            this.Player6Card2Box.Name = "Player6Card2Box";
            this.Player6Card2Box.Size = new System.Drawing.Size(72, 90);
            this.Player6Card2Box.TabIndex = 3;
            this.Player6Card2Box.TabStop = false;
            // 
            // Player2Card2Box
            // 
            this.Player2Card2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player2Card2Box.Image = ((System.Drawing.Image)(resources.GetObject("Player2Card2Box.Image")));
            this.Player2Card2Box.Location = new System.Drawing.Point(206, 418);
            this.Player2Card2Box.Name = "Player2Card2Box";
            this.Player2Card2Box.Size = new System.Drawing.Size(72, 90);
            this.Player2Card2Box.TabIndex = 3;
            this.Player2Card2Box.TabStop = false;
            // 
            // Player6Card1Box
            // 
            this.Player6Card1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player6Card1Box.Image = ((System.Drawing.Image)(resources.GetObject("Player6Card1Box.Image")));
            this.Player6Card1Box.Location = new System.Drawing.Point(726, 418);
            this.Player6Card1Box.Name = "Player6Card1Box";
            this.Player6Card1Box.Size = new System.Drawing.Size(72, 90);
            this.Player6Card1Box.TabIndex = 3;
            this.Player6Card1Box.TabStop = false;
            // 
            // Player2Card1Box
            // 
            this.Player2Card1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player2Card1Box.Image = ((System.Drawing.Image)(resources.GetObject("Player2Card1Box.Image")));
            this.Player2Card1Box.Location = new System.Drawing.Point(128, 418);
            this.Player2Card1Box.Name = "Player2Card1Box";
            this.Player2Card1Box.Size = new System.Drawing.Size(72, 90);
            this.Player2Card1Box.TabIndex = 3;
            this.Player2Card1Box.TabStop = false;
            // 
            // Player4Card2Box
            // 
            this.Player4Card2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player4Card2Box.Image = ((System.Drawing.Image)(resources.GetObject("Player4Card2Box.Image")));
            this.Player4Card2Box.Location = new System.Drawing.Point(504, 123);
            this.Player4Card2Box.Name = "Player4Card2Box";
            this.Player4Card2Box.Size = new System.Drawing.Size(72, 90);
            this.Player4Card2Box.TabIndex = 3;
            this.Player4Card2Box.TabStop = false;
            // 
            // Player4Card1Box
            // 
            this.Player4Card1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player4Card1Box.Image = ((System.Drawing.Image)(resources.GetObject("Player4Card1Box.Image")));
            this.Player4Card1Box.Location = new System.Drawing.Point(426, 123);
            this.Player4Card1Box.Name = "Player4Card1Box";
            this.Player4Card1Box.Size = new System.Drawing.Size(72, 90);
            this.Player4Card1Box.TabIndex = 3;
            this.Player4Card1Box.TabStop = false;
            // 
            // Player1Card2Box
            // 
            this.Player1Card2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player1Card2Box.Image = global::PokerSimulator.Properties.Resources.BackSide;
            this.Player1Card2Box.Location = new System.Drawing.Point(504, 495);
            this.Player1Card2Box.Name = "Player1Card2Box";
            this.Player1Card2Box.Size = new System.Drawing.Size(72, 90);
            this.Player1Card2Box.TabIndex = 3;
            this.Player1Card2Box.TabStop = false;
            // 
            // RiverCardBox
            // 
            this.RiverCardBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.RiverCardBox.Image = ((System.Drawing.Image)(resources.GetObject("RiverCardBox.Image")));
            this.RiverCardBox.Location = new System.Drawing.Point(623, 299);
            this.RiverCardBox.Name = "RiverCardBox";
            this.RiverCardBox.Size = new System.Drawing.Size(72, 90);
            this.RiverCardBox.TabIndex = 3;
            this.RiverCardBox.TabStop = false;
            // 
            // TurnCardBox
            // 
            this.TurnCardBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TurnCardBox.Image = ((System.Drawing.Image)(resources.GetObject("TurnCardBox.Image")));
            this.TurnCardBox.Location = new System.Drawing.Point(545, 299);
            this.TurnCardBox.Name = "TurnCardBox";
            this.TurnCardBox.Size = new System.Drawing.Size(72, 90);
            this.TurnCardBox.TabIndex = 3;
            this.TurnCardBox.TabStop = false;
            // 
            // FlopCard3Box
            // 
            this.FlopCard3Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FlopCard3Box.Image = ((System.Drawing.Image)(resources.GetObject("FlopCard3Box.Image")));
            this.FlopCard3Box.Location = new System.Drawing.Point(467, 299);
            this.FlopCard3Box.Name = "FlopCard3Box";
            this.FlopCard3Box.Size = new System.Drawing.Size(72, 90);
            this.FlopCard3Box.TabIndex = 3;
            this.FlopCard3Box.TabStop = false;
            // 
            // FlopCard2Box
            // 
            this.FlopCard2Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FlopCard2Box.Image = ((System.Drawing.Image)(resources.GetObject("FlopCard2Box.Image")));
            this.FlopCard2Box.Location = new System.Drawing.Point(389, 299);
            this.FlopCard2Box.Name = "FlopCard2Box";
            this.FlopCard2Box.Size = new System.Drawing.Size(72, 90);
            this.FlopCard2Box.TabIndex = 3;
            this.FlopCard2Box.TabStop = false;
            // 
            // FlopCard1Box
            // 
            this.FlopCard1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FlopCard1Box.Image = ((System.Drawing.Image)(resources.GetObject("FlopCard1Box.Image")));
            this.FlopCard1Box.Location = new System.Drawing.Point(311, 299);
            this.FlopCard1Box.Name = "FlopCard1Box";
            this.FlopCard1Box.Size = new System.Drawing.Size(72, 90);
            this.FlopCard1Box.TabIndex = 3;
            this.FlopCard1Box.TabStop = false;
            // 
            // Player1Card1Box
            // 
            this.Player1Card1Box.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Player1Card1Box.Image = global::PokerSimulator.Properties.Resources.BackSide;
            this.Player1Card1Box.Location = new System.Drawing.Point(426, 495);
            this.Player1Card1Box.Name = "Player1Card1Box";
            this.Player1Card1Box.Size = new System.Drawing.Size(72, 90);
            this.Player1Card1Box.TabIndex = 3;
            this.Player1Card1Box.TabStop = false;
            // 
            // Player1PositionBox
            // 
            this.Player1PositionBox.BackColor = System.Drawing.Color.Transparent;
            this.Player1PositionBox.Location = new System.Drawing.Point(426, 459);
            this.Player1PositionBox.Name = "Player1PositionBox";
            this.Player1PositionBox.Size = new System.Drawing.Size(30, 30);
            this.Player1PositionBox.TabIndex = 4;
            this.Player1PositionBox.TabStop = false;
            // 
            // Player2PositionBox
            // 
            this.Player2PositionBox.BackColor = System.Drawing.Color.Transparent;
            this.Player2PositionBox.Location = new System.Drawing.Point(248, 382);
            this.Player2PositionBox.Name = "Player2PositionBox";
            this.Player2PositionBox.Size = new System.Drawing.Size(30, 30);
            this.Player2PositionBox.TabIndex = 4;
            this.Player2PositionBox.TabStop = false;
            // 
            // Player3PositionBox
            // 
            this.Player3PositionBox.BackColor = System.Drawing.Color.Transparent;
            this.Player3PositionBox.Location = new System.Drawing.Point(248, 276);
            this.Player3PositionBox.Name = "Player3PositionBox";
            this.Player3PositionBox.Size = new System.Drawing.Size(30, 30);
            this.Player3PositionBox.TabIndex = 4;
            this.Player3PositionBox.TabStop = false;
            // 
            // Player4PositionBox
            // 
            this.Player4PositionBox.BackColor = System.Drawing.Color.Transparent;
            this.Player4PositionBox.Location = new System.Drawing.Point(426, 219);
            this.Player4PositionBox.Name = "Player4PositionBox";
            this.Player4PositionBox.Size = new System.Drawing.Size(30, 30);
            this.Player4PositionBox.TabIndex = 4;
            this.Player4PositionBox.TabStop = false;
            // 
            // Player5PositionBox
            // 
            this.Player5PositionBox.BackColor = System.Drawing.Color.Transparent;
            this.Player5PositionBox.Location = new System.Drawing.Point(726, 276);
            this.Player5PositionBox.Name = "Player5PositionBox";
            this.Player5PositionBox.Size = new System.Drawing.Size(30, 30);
            this.Player5PositionBox.TabIndex = 4;
            this.Player5PositionBox.TabStop = false;
            // 
            // Player6PositionBox
            // 
            this.Player6PositionBox.BackColor = System.Drawing.Color.Transparent;
            this.Player6PositionBox.Location = new System.Drawing.Point(726, 382);
            this.Player6PositionBox.Name = "Player6PositionBox";
            this.Player6PositionBox.Size = new System.Drawing.Size(30, 30);
            this.Player6PositionBox.TabIndex = 4;
            this.Player6PositionBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(541, 479);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 7;
            // 
            // replayWindowButton
            // 
            this.replayWindowButton.BackColor = System.Drawing.Color.Red;
            this.replayWindowButton.ForeColor = System.Drawing.Color.Yellow;
            this.replayWindowButton.Location = new System.Drawing.Point(907, 662);
            this.replayWindowButton.Name = "replayWindowButton";
            this.replayWindowButton.Size = new System.Drawing.Size(89, 35);
            this.replayWindowButton.TabIndex = 0;
            this.replayWindowButton.Text = "Replay";
            this.replayWindowButton.UseVisualStyleBackColor = false;
            this.replayWindowButton.Click += new System.EventHandler(this.replayWindowButton_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Player6PositionBox);
            this.Controls.Add(this.Player5PositionBox);
            this.Controls.Add(this.Player4PositionBox);
            this.Controls.Add(this.Player3PositionBox);
            this.Controls.Add(this.Player2PositionBox);
            this.Controls.Add(this.Player1PositionBox);
            this.Controls.Add(this.Player5Card2Box);
            this.Controls.Add(this.Player3Card2Box);
            this.Controls.Add(this.Player5Card1Box);
            this.Controls.Add(this.Player3Card1Box);
            this.Controls.Add(this.Player6Card2Box);
            this.Controls.Add(this.Player2Card2Box);
            this.Controls.Add(this.Player6Card1Box);
            this.Controls.Add(this.Player2Card1Box);
            this.Controls.Add(this.Player4Card2Box);
            this.Controls.Add(this.Player4Card1Box);
            this.Controls.Add(this.Player1Card2Box);
            this.Controls.Add(this.RiverCardBox);
            this.Controls.Add(this.TurnCardBox);
            this.Controls.Add(this.FlopCard3Box);
            this.Controls.Add(this.FlopCard2Box);
            this.Controls.Add(this.FlopCard1Box);
            this.Controls.Add(this.Player1Card1Box);
            this.Controls.Add(this.player6Name);
            this.Controls.Add(this.player5Name);
            this.Controls.Add(this.player2Name);
            this.Controls.Add(this.player3Name);
            this.Controls.Add(this.player4Name);
            this.Controls.Add(this.player1Name);
            this.Controls.Add(this.replayWindowButton);
            this.Controls.Add(this.ClearAllCards);
            this.Controls.Add(this.DealRiverCard);
            this.Controls.Add(this.DealTurnCard);
            this.Controls.Add(this.DealFlopCards);
            this.Controls.Add(this.ShuffleDeck);
            this.Controls.Add(this.DealCardsToPlayers);
            this.Location = new System.Drawing.Point(25, 25);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.Text = "Poker Simulator v1.2";
            ((System.ComponentModel.ISupportInitialize)(this.Player5Card2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Card2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player5Card1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Card1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player6Card2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Card2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player6Card1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Card1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Card2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Card1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Card2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RiverCardBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TurnCardBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FlopCard3Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FlopCard2Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FlopCard1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Card1Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1PositionBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2PositionBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3PositionBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4PositionBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player5PositionBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player6PositionBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button DealCardsToPlayers;
        private System.Windows.Forms.Label player1Name;
        private System.Windows.Forms.Label player4Name;
        private System.Windows.Forms.Label player3Name;
        private System.Windows.Forms.Label player2Name;
        private System.Windows.Forms.Label player5Name;
        private System.Windows.Forms.Label player6Name;
        private System.Windows.Forms.Button DealFlopCards;
        private System.Windows.Forms.Button ShuffleDeck;
        private System.Windows.Forms.Button DealTurnCard;
        private System.Windows.Forms.Button DealRiverCard;
        private System.Windows.Forms.Button ClearAllCards;
        private System.Windows.Forms.PictureBox Player1Card1Box;
        private System.Windows.Forms.PictureBox Player1Card2Box;
        private System.Windows.Forms.PictureBox Player2Card1Box;
        private System.Windows.Forms.PictureBox Player2Card2Box;
        private System.Windows.Forms.PictureBox Player3Card1Box;
        private System.Windows.Forms.PictureBox Player3Card2Box;
        private System.Windows.Forms.PictureBox Player4Card1Box;
        private System.Windows.Forms.PictureBox Player4Card2Box;
        private System.Windows.Forms.PictureBox Player6Card1Box;
        private System.Windows.Forms.PictureBox Player6Card2Box;
        private System.Windows.Forms.PictureBox Player5Card1Box;
        private System.Windows.Forms.PictureBox Player5Card2Box;
        private System.Windows.Forms.PictureBox FlopCard1Box;
        private System.Windows.Forms.PictureBox FlopCard2Box;
        private System.Windows.Forms.PictureBox FlopCard3Box;
        private System.Windows.Forms.PictureBox TurnCardBox;
        private System.Windows.Forms.PictureBox RiverCardBox;
        private System.Windows.Forms.PictureBox Player1PositionBox;
        private System.Windows.Forms.PictureBox Player2PositionBox;
        private System.Windows.Forms.PictureBox Player3PositionBox;
        private System.Windows.Forms.PictureBox Player4PositionBox;
        private System.Windows.Forms.PictureBox Player5PositionBox;
        private System.Windows.Forms.PictureBox Player6PositionBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button replayWindowButton;
    }
}

