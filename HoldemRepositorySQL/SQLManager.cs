﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HoldemTable;
using System.Data.SqlClient;

namespace HoldemRepositorySQL
{
    public class SQLManager
    {
        private Table table = new Table();
        private Player[] playerCards;
        private BoardCards boardCards;
        
        public void GetGameCards()
        {
            playerCards = table.Players;
            boardCards = table.Board;
        }

        public void SaveGameCards(Player[] pc, BoardCards bc)
        {
            GetGameCards();
            SqlConnection myConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\C sharp\GitTestRepository\PokerSimulator\pokersimulator\HoldemRepositorySQL\GameDatabase.mdf;Integrated Security=True");
            myConnection.Open();

            SqlCommand myCommand = new SqlCommand("INSERT INTO HoldemTable (DealerPosition,Player1Card1,Player1Card2,Player2Card1,Player2Card2,Player3Card1,Player3Card2," +
                "                                                           Player4Card1,Player4Card2,Player5Card1,Player5Card2,Player6Card1,Player6Card2)" +
                "                                   VALUES (@DealerPosition,@Player1Card1,@Player1Card2,@Player2Card1,@Player2Card2,@Player3Card1,@Player3Card2," +
                "                                                           @Player4Card1,@Player4Card2,@Player5Card1,@Player5Card2,@Player6Card1,@Player6Card2)", myConnection);
            myCommand.Parameters.AddWithValue("@DealerPosition", "dp");
            myCommand.Parameters.AddWithValue("@Player1Card1", pc[0].Card1.ToString());
            myCommand.Parameters.AddWithValue("@Player1Card2", pc[0].Card2.ToString());
            myCommand.Parameters.AddWithValue("@Player2Card1", pc[1].Card1.ToString());
            myCommand.Parameters.AddWithValue("@Player2Card2", pc[1].Card2.ToString());
            myCommand.Parameters.AddWithValue("@Player3Card1", pc[2].Card1.ToString());
            myCommand.Parameters.AddWithValue("@Player3Card2", pc[2].Card2.ToString());
            myCommand.Parameters.AddWithValue("@Player4Card1", pc[3].Card1.ToString());
            myCommand.Parameters.AddWithValue("@Player4Card2", pc[3].Card2.ToString());
            myCommand.Parameters.AddWithValue("@Player5Card1", pc[4].Card1.ToString());
            myCommand.Parameters.AddWithValue("@Player5Card2", pc[4].Card2.ToString());
            myCommand.Parameters.AddWithValue("@Player6Card1", pc[5].Card1.ToString());
            myCommand.Parameters.AddWithValue("@Player6Card2", pc[5].Card2.ToString());
            myCommand.ExecuteNonQuery();
            
        }
       

    }
}
