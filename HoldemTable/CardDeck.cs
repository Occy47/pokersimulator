﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoldemTable
{
    public class CardDeck
    {

        public Card[] cardDeck;


        public CardDeck()
        {
            cardDeck = new Card[52];
            var ranks = (Card.CardRank[])Enum.GetValues(typeof(Card.CardRank));
            var suits = (Card.CardSuite[])Enum.GetValues(typeof(Card.CardSuite));

            var cards = new List<Card>();
                foreach (var s in suits)
                {
                    foreach (var r in ranks)
                    {

                    cards.Add(new Card(r, s));

                    }
                }

            cardDeck = cards.ToArray();

        }




    }
}
