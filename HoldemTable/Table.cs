﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoldemTable
{
    public class Table
    {
        private Card[] shuffledDeck;
        public Card[] ShuffledDeck
        {
            get { return shuffledDeck; }
            set { shuffledDeck = value; }
        }

        private Player[] players;
        public Player[] Players
        {
            get { return players; }
            set { players = value; }
        }

        private BoardCards board;
        public BoardCards Board
        {
            get { return board; }
            set { board = value; }
        }

        public PlayerPosition[] currentPosition;

        private Queue<PlayerPosition> tablePositions = new Queue<PlayerPosition>();

        public enum PlayerPosition
        {
            Dealer,
            SmallBlind,
            BigBlind,
            UnderTheGun,
            UnderTheGunPlus1,
            CutOff
        }

        public void ShuffleCardDeck()
        {
            Random rnd = new Random();
            var deck = new CardDeck();
            shuffledDeck = deck.cardDeck.OrderBy(x => rnd.Next()).ToArray();
        }

        public void SetPositions()
        {
            tablePositions.Enqueue(PlayerPosition.Dealer);
            tablePositions.Enqueue(PlayerPosition.SmallBlind);
            tablePositions.Enqueue(PlayerPosition.BigBlind);
            tablePositions.Enqueue(PlayerPosition.UnderTheGun);
            tablePositions.Enqueue(PlayerPosition.UnderTheGunPlus1);
            tablePositions.Enqueue(PlayerPosition.CutOff);

            currentPosition = tablePositions.ToArray();
        }

        public Player GetCardsToPlayer(PlayerPosition pos)
        {
            Player result = new Player();

            if (pos == PlayerPosition.Dealer)
                result = new Player(ShuffledDeck[0], ShuffledDeck[6]);
            else if (pos == PlayerPosition.SmallBlind)
                result = new Player(ShuffledDeck[1], ShuffledDeck[7]);
            else if (pos == PlayerPosition.BigBlind)
                result = new Player(ShuffledDeck[2], ShuffledDeck[8]);
            else if (pos == PlayerPosition.UnderTheGun)
                result = new Player(ShuffledDeck[3], ShuffledDeck[9]);
            else if (pos == PlayerPosition.UnderTheGunPlus1)
                result = new Player(ShuffledDeck[4], ShuffledDeck[10]);
            else if (pos == PlayerPosition.CutOff)
                result = new Player(ShuffledDeck[5], ShuffledDeck[11]);
            return result;
        }

        public void SetPlayers()
        {
            players = new Player[6];

            players[0] = GetCardsToPlayer(currentPosition[5]);
            players[1] = GetCardsToPlayer(currentPosition[4]);
            players[2] = GetCardsToPlayer(currentPosition[3]);
            players[3] = GetCardsToPlayer(currentPosition[2]);
            players[4] = GetCardsToPlayer(currentPosition[1]);
            players[5] = GetCardsToPlayer(currentPosition[0]);
        }

        public void AssignCardsToBoard()
        {
            board.Card1 = ShuffledDeck[13];
            board.Card2 = ShuffledDeck[14];
            board.Card3 = ShuffledDeck[15];
            board.Card4 = ShuffledDeck[17];
            board.Card5 = ShuffledDeck[19];
        }

        public void SetNextPosition()
        {
            PlayerPosition pos;
            pos = tablePositions.Dequeue();
            tablePositions.Enqueue(pos);
            currentPosition = tablePositions.ToArray();
        }





    }
}