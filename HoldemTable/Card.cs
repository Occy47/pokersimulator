﻿using System;

namespace HoldemTable
{
    public class Card 
    {
        public Card(CardRank cardRank, CardSuite cardSuite)
        {
            Rank = cardRank;
            Suite = cardSuite;
        }

        [Flags]
        public enum CardSuite
        {
            Spades,
            Hearts,
            Clubs,
            Diamonds
        };

        [Flags]
        public enum CardRank
        {
            Two = 102,
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine,
            Ten ,
            Jack,
            Queen,
            King,
            Ace
        };

        public CardSuite Suite { get; }
        public CardRank Rank { get; }

        public override string ToString()
        {
        
            return Rank + " \r\nof " + Suite;
        }

    }


}
