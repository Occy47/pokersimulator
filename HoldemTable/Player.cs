﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoldemTable
{
    public class Player
    {
        public Player()
        {

        }
        public Player( Card card1, Card card2)
        {

            Card1 = card1;
            Card2 = card2;
        }


        public Card Card1 { get; set; }
        public Card Card2 { get; set; }
    }
}
